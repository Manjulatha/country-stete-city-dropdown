async function fetchData(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

function populateDropdown(dropdown, data, filterText) {
    dropdown.innerHTML = '<option value="">Select</option>';

    data.forEach(item => {
        if (item.name.toLowerCase().includes(filterText.toLowerCase())) {
            const option = document.createElement('option');
            option.value = item.name;
            option.text = item.name;
            dropdown.appendChild(option);
        }
    });
}

async function populateCountryDropdown() {
    const countries = await fetchData('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries%2Bstates%2Bcities.json');
    const countryDropdown = document.getElementById('country');
    const countryFilterInput = document.getElementById('countryFilter');

    countryFilterInput.addEventListener('input', () => {
        populateDropdown(countryDropdown, countries, countryFilterInput.value);
    });

    populateDropdown(countryDropdown, countries, '');
}

async function populateStateDropdown(selectedCountry) {
    const countries = await fetchData('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries%2Bstates%2Bcities.json');
    const stateDropdown = document.getElementById('state');
    const stateFilterInput = document.getElementById('stateFilter');

    stateFilterInput.addEventListener('input', () => {
        const countryData = countries.find(country => country.name === selectedCountry);
        const states = countryData ? countryData.states : [];
        populateDropdown(stateDropdown, states, stateFilterInput.value);
    });

    const countryData = countries.find(country => country.name === selectedCountry);
    const states = countryData ? countryData.states : [];
    populateDropdown(stateDropdown, states, '');
}

async function populateCityDropdown(selectedCountry, selectedState) {
    const countries = await fetchData('https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries%2Bstates%2Bcities.json');
    const cityDropdown = document.getElementById('city');
    const cityFilterInput = document.getElementById('cityFilter');

    cityFilterInput.addEventListener('input', () => {
        const countryData = countries.find(country => country.name === selectedCountry);
        const stateData = countryData ? countryData.states.find(state => state.name === selectedState) : null;
        const cities = stateData ? stateData.cities : [];
        populateDropdown(cityDropdown, cities, cityFilterInput.value);
    });

    const countryData = countries.find(country => country.name === selectedCountry);
    const stateData = countryData ? countryData.states.find(state => state.name === selectedState) : null;
    const cities = stateData ? stateData.cities : [];
    populateDropdown(cityDropdown, cities, '');
}

populateCountryDropdown();

const countryDropdown = document.getElementById('country');
const stateDropdown = document.getElementById('state');
const cityDropdown = document.getElementById('city');

countryDropdown.addEventListener('change', () => {
    const selectedCountry = countryDropdown.value;
    populateStateDropdown(selectedCountry);
    cityDropdown.innerHTML = '<option value="">Select</option>';
});

stateDropdown.addEventListener('change', () => {
    const selectedCountry = countryDropdown.value;
    const selectedState = stateDropdown.value;
    populateCityDropdown(selectedCountry, selectedState);
});
